package br.com.mastertech.kafka.cadastro;

import br.com.mastertech.kafka.model.ResultadoValidacaoEmpresa;
import br.com.mastertech.kafka.service.RegistroArquivoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class ResultadoValidacaoListener {

    public static final String TOPICO_RESULTADO_AUDIT = "pedro-biro-2";

    @Autowired
    private RegistroArquivoService registroArquivoService;

    @KafkaListener(topics = TOPICO_RESULTADO_AUDIT, groupId = "grupo1")
    public void registraResultado(@Payload ResultadoValidacaoEmpresa resultado) {
        registroArquivoService.registrar(resultado);
    }
}
