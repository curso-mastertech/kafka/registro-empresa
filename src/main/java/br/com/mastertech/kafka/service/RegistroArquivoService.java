package br.com.mastertech.kafka.service;

import br.com.mastertech.kafka.model.ResultadoValidacaoEmpresa;
import org.springframework.stereotype.Service;

@Service
public class RegistroArquivoService {

    public void registrar(ResultadoValidacaoEmpresa resultado) {
        System.out.printf("CNPJ: %s - %s\n", resultado.getCnpj(), resultado.isCapitalAceito() ? "ACEITO" : "NEGADO");
    }
}